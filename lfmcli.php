<?php
/**
 * PHP CLI for LastFM
 *
 */

$phpVersion = preg_replace('/[^0-9]/', '', PHP_VERSION);
$phpVersion = (int) substr($phpVersion, 0, 2);
if ($phpVersion < 71) {
    die('Sorry, but at least PHP version 7.1 is required'.PHP_EOL);
}

require_once 'src/lfmcli.php';

$limit      = 10;
$command    = '';

$opts  = array(
    'lt::',
    'la::',
    'tt::',
    'ta::',
);
$options = getopt('', $opts);

if (array_key_exists('lt', $options)) {
    $command = 'lt';
} elseif (array_key_exists('la', $options)) {
    $command = 'la';
} elseif (array_key_exists('tt', $options)) {
    $command = 'tt';
} elseif (array_key_exists('ta', $options)) {
    $command = 'ta';
}


$lfmcli = new Lfmcli($limit);

$output = '';
if ($command == 'lt') {
    $output = $lfmcli->getMyLatestTracks();
} elseif($command == 'la') {
    $output = $lfmcli->getMyLatestArtists();
} elseif($command == 'tt') {
    $output = $lfmcli->getMyTopTracks();
} elseif($command == 'ta') {
    $output = 'Top Artists'.PHP_EOL;
} else {
    $output = $lfmcli->getIntro();
}

echo $output;

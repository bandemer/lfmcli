<?php
/**
 * Class Lfmcli
 */
class Lfmcli
{
    /**
     * @var LastFM API host name
     */
    private $api_host;

    /**
     * @var LastFM API Key
     */
    private $api_key;

    /**
     * @var int Limit
     */
    private $limit;

    /**
     * @var LastFM User Name
     */
    private $me;

    /**
     * curl handle
     */
    private $curl;

    /**
     * Lfmcli constructor.
     * @param int $limit
     */
    public function __construct($limit = 10)
    {
        require_once 'keys.php';
        require_once 'config.php';

        $this->api_host = $lfc_api_host;
        $this->api_key  = $lfc_api_key;
        $this->limit    = (int) $limit;
        $this->me       = $lfc_me;

        $this->curl = curl_init();
    }

    /**
     * Desctructor
     */
    public function __destruct()
    {
        if (is_resource($this->curl)) {
            curl_close($this->curl);
        }
    }


    /**
     * Get latest Scrobbles
     * @return string
     */
    public function getMyLatestTracks()
    {
        $output = '';

        curl_setopt_array($this->curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->api_host.'?method=user.getrecenttracks&api_key='.$this->api_key.
                '&limit='.$this->limit.
                '&user='.$this->me.
                '&format=json'
        ]);

        $response = curl_exec($this->curl);

        $output .= 'These are your recent '.$this->limit.' scrobbled tracks:'.PHP_EOL.PHP_EOL;

        $latest = json_decode($response, true);

        $counter = 1;
        foreach($latest['recenttracks']['track'] AS $t) {

            $output .= str_pad(strval($counter), strlen(strval($this->limit)),
                    ' ', STR_PAD_LEFT).': '.$t['artist']['#text'].chr(9).$t['name'].PHP_EOL;
            ++$counter;
        }

        return $output;
    }

    /**
     * Get latest artists
     * @return string
     */
    public function getMyLatestArtists()
    {
        $returnString = '';

        $artists = [];

        $page = 0;
        while (count($artists) <= $this->limit) {


            curl_setopt_array($this->curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $this->api_host . '?method=user.getrecenttracks&api_key=' . $this->api_key .
                    '&limit=' . $this->limit .
                    '&user=' . $this->me .
                    '&page=' . $page .
                    '&format=json'
            ]);

            $response = curl_exec($this->curl);

            $latest = json_decode($response, true);

            if (array_key_exists('recenttracks', $latest)) {
                foreach ($latest['recenttracks']['track'] AS $t) {
                    $id = md5($t['artist']['#text']);
                    $artists[$id] = $t['artist']['#text'];
                }
            }
            ++$page;
        }

        $artists = array_reverse($artists);

        $counter = 0;
        while($counter < $this->limit) {

            $a = array_pop($artists);
            $returnString .= str_pad(strval($counter+1), strlen(strval($this->limit)), ' ', STR_PAD_LEFT) .
                ': ' . $a . PHP_EOL;
            ++$counter;
        }

        return $returnString;
    }

    /**
     * Get Intro
     * @return string
     */
    public function getIntro()
    {
        curl_setopt_array($this->curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->api_host . '?method=user.getinfo&api_key=' . $this->api_key .
                '&user=' . $this->me .
                '&format=json'
        ]);

        $response = curl_exec($this->curl);

        $user = json_decode($response, true);

        $output = PHP_EOL.'Hello '.$user['user']['realname'].'! Welcome to fmCLI'."\u{266B}".PHP_EOL.PHP_EOL.

            'These are your options:'.PHP_EOL.PHP_EOL.
            '--lt'.chr(9).' Get latest tracks'.PHP_EOL.
            '--la'.chr(9).' Get latest artists'.PHP_EOL.
            '--tt'.chr(9).' Get top tracks'.PHP_EOL.
            '--ta'.chr(9).' Get top artists'.PHP_EOL;

        return $output;
    }

    /**
     * Get My Top Tracks
     */
    public function getMyTopTracks()
    {
        /*
         * Get API data
         */
        curl_setopt_array($this->curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->api_host.'?method=user.gettoptracks&api_key='.$this->api_key.
                '&limit='.$this->limit.
                '&user='.$this->me.
                '&period=7day'.
                '&format=json'
        ]);

        $response = curl_exec($this->curl);

        $output = PHP_EOL.'These are your top '.$this->limit.' tracks of the last 7 days:'.PHP_EOL.PHP_EOL;

        $top = json_decode($response, true);

        /**
         * Create array with top tracks
         */
        $topTracks = [];
        $maxLength = 0;

        foreach($top['toptracks']['track'] AS $t) {

            $topTracks[intval($t['@attr']['rank'])] = [
                'artist' => $t['artist']['name'],
                'track'  => $t['name'],
            ];

            $maxLength = (strlen($t['artist']['name']) > $maxLength) ? strlen($t['artist']['name']) : $maxLength;
        }


        /**
         * Create output
         */
        foreach($topTracks AS $tkey => $t) {

            //Rank
            $line = str_pad(strval($tkey), strlen(strval($this->limit)), ' ', STR_PAD_LEFT) . ': ';

            //Artist
            $line .= $t['artist'];

            //Spaces
            $line .= str_repeat(' ', $maxLength - (int) floor(strlen($t['artist'])));

            //Track
            $line .= ' - ' . $t['track'].PHP_EOL;

            //Limit to max 80 signs
            $output .= (strlen($line) > 80) ? mb_substr($line, 0, 77).'...' : $line;
        }

        return $output;
    }
}